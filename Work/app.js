// Module dependencies.
var express = require("express")
  , http = require("http")
  , path = require("path")
  , routes = require("./routes");
var app = express();
var soap = require('soap');

// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes
app.get("/", getBreadcrumbs, routes.home);
app.get("/home", getBreadcrumbs, routes.home);
app.get("/subCategories", getBreadcrumbs, routes.subCategoriesGet);
app.get("/products", getBreadcrumbs, routes.products);
app.get("/product", getBreadcrumbs, routes.product);


var args = { name: 'value' };
soap.createClient('index', function (err, client) {

});






// Run server
http.createServer(app).listen(app.get("port"), function () {
  console.log("Express server listening on port " + app.get("port"));
});


function getBreadcrumbs(req, res, next) {
  const urls = req.originalUrl.split('/');
  urls.shift();
  req.breadcrumbs = urls.map((url, i) => {
    return {
      breadcrumbName: (url === '' ? 'Home' : url.charAt(0).toUpperCase() + url.slice(1)),
      breadcrumbUrl: `/${urls.slice(0, i + 1).join('/')}`,
    };
  });
  next();
}