exports.index = function (req, res) {
	res.render("index", {
		// Template data
		title: "Express"
	});
};


exports.home = function (req, res) {
	var _ = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	mdbClient.connect("mongodb://localhost:27017/", function (err, client) {
		var db = client.db('shop');
		var collection = db.collection('categories');

		collection.find().toArray(function (err, items) {
			res.render("home", {
				// Underscore.js lib
				_: _,

				// Template data
				title: "Home",
				breadcrumbs: req.breadcrumbs,
				items: items
			});


			client.close();
		});
	});
};

exports.subCategoriesGet = function (req, res) {
	var _ = require("underscore");
	var topCId = req.query.topCId;
	var subCId = req.query.subCId;
	var mdbClient = require('mongodb').MongoClient;

	mdbClient.connect("mongodb://localhost:27017/", function (err, client) {
		var db = client.db('shop');
		var collection = db.collection('categories');

		collection.aggregate(
			{ $match: { id: topCId } },

			{ $unwind: "$categories" },

			{ $match: { "categories.id": subCId } },

			{
				$project: {
					_id: 0,
					id: "$categories.id",
					name: "$categories.name"
				}
			}

		).toArray(function (err, items) {


			if (!topCId || !subCId) {
				notFound(res);
				return;
			}


			res.render("subCategories", {
				// Underscore.js lib
				_: _,

				// Template data
				title: subCId.charAt(0).toUpperCase() + subCId.slice(1),
				items: items,
				breadcrumbs: req.breadcrumbs
			});
			client.close();
		});
	});
};
exports.products = function (req, res) {
	var productCId = req.query.productCId;
	var desc = parseInt(req.query.desc, 10);
	var filter = req.query.f;
	var _ = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	mdbClient.connect("mongodb://localhost:27017/", function (err, client) {
		var db = client.db('shop');
		var collection = db.collection('products');

		collection.find({ primary_category_id: productCId }).sort({ price: desc }).toArray(function (err, items) {

			if (err) {
				if (!productCId || !desc) {
					notFound(res);
					return;
				}
			}
			if (filter) {
				items = items.filter(word => word.name.includes(filter));
			}
			res.render("products", {
				// Underscore.js lib
				_: _,

				// Template data
				title: productCId.charAt(0).toUpperCase() + productCId.slice(1),
				items: items,
				filter: filter,
				breadcrumbs: req.breadcrumbs,
				productCId: productCId
			});
			client.close();
		});
	});
};


exports.product = function (req, res) {
	var _ = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var product_id = req.query.id;

	mdbClient.connect("mongodb://localhost:27017/", function (err, client) {
		var db = client.db('shop');
		var collection = db.collection('products');

		collection.find({ id: product_id }).toArray(function (err, items) {
			res.render("product", {
				// Underscore.js lib
				_: _,

				// Template data
				title: items[0].name,
				item: items[0]
			});
			console.log(items[0].variants);
			client.close();
		});
	});
};

// HTTP status 404: NotFound
function notFound(res) {
	res.status(404).send({ error: "404 NotFound" });
}
